import { Fragment } from 'react';
import InputHolder from '../InputHolder/InputHolder';
import SumIn from '../SumIn/SumIn'
import './App.scss';

function App() {
  return (
    <Fragment>
      <main>
        <InputHolder />
        <SumIn/>
      </main>
    </Fragment>
  );
}

export default App;
