
export function add(number) {
  return {
    type: 'ADD',
    payload: {
      number,
    },
  };
}

