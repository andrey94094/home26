import React from 'react';
import { Provider } from 'react-redux'; // это специальная штука, которая и займется "знакоством" (работает на основе контекста (1))
import store from './store'; // это наш стор который мы сделали еще в начале
import ReactDOM from 'react-dom';
import './index.scss';
import App from './components/App/App';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
