import React, { Component } from 'react';
import { connect } from 'react-redux';
import { add } from '../../actionCreators';
import './InputHolder.scss';

class InputHolder extends Component {
  state = {
    value: 0,
  };

  handleChange = (e) => {
    if (isNaN(e.target.value)) {
      return;
    }
    this.setState({ value: Number(e.target.value) });
  };


  handleClick = () => {
    const { value } = this.state;
    const { add } = this.props;
    if (!value) {
      this.setState({ error: 'нельзя пустое!' });
      return;
    }
    add(value);
    this.setState({ value: '' });
  };

  render() {
    const { value, error } = this.state;
    return (
      <div className='wrapper'>
        <input value={value} onChange={this.handleChange} />
        <button onClick={this.handleClick} disabled={error}>
          добавить
        </button>
        <div className='error'>{error}</div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  add: (number) => dispatch(add(number)),
});

export default connect(null, mapDispatchToProps)(InputHolder);
