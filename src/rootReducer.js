const initState = {
  count: 0,
  sum: 0,
};

function reducer(state = initState, action) {
  switch (action.type) {

    case 'ADD': {
      const { number } = action.payload;
      return { ...state, sum: state.sum + number };
    }

    default:
      return state;
  }
}

export default reducer;

